let getNumberN=+prompt('Enter number n');

const fibo = n=>{

   if (n<=0) {
       return [0, 1];
   } else {
       const [Fo,F1]=fibo(n-1);
       return [F1,F0+F1];
   }
};

const fibo2 = n=> fibo(n)[0];
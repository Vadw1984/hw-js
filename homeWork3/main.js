
function mathOperate(num1, num2, operation) {
    do {
        num1 = +prompt('Enter first number', '');

    } while (
        num1 === '' ||
        num1 === ' ' ||
        num1 === null ||
        isNaN(num1));
    do {
        num2 = +prompt('Enter second number', '');
    } while (
        num2 === '' ||
        num2 === ' ' ||
        num2 === null ||
        isNaN(num2));

    do {
        operation = prompt('Enter operation', '');

    } while (
        operation === '' ||
        operation === ' ' ||
        operation === null
        );

    let result=null;
    switch (operation) {
        case '+':
            result = num1 + +num2;
            break;
        case '-':
            result = num1 - num2;
            break;
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
    }
     return (result)
}
console.log(mathOperate());

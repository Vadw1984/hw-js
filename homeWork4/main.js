// const product = {
//     name:"Ноутбук Acer",
//     "full name":"Ноутбук Acer Swift 3 SF314-52 (NX.GNUEU.038)",
//     price: 19048.5,
//     availability: false,
//     "additional gift": null
// };
//
// console.log(product["availability"]);

// const student = {
//     name: "Григорий",
//     "last name": "Сковорода",
//     laziness: 4,
//     trick: 3
// };
//
// console.log(student);
//
//
//
// if (student.laziness >= 3 && student.laziness <= 5 && student.trick <= 4)
// {console.log(`Cтудент ${student.name} ${student["last name"]} отправлен на пересдачу`)}


// function loop(x) {
//     if (x >= 10) // "x >= 10" — это условие для конца выполения (тоже самое, что "!(x < 10)")
//         return;
//     // делать что-то
//     loop(x + 1); // рекурсионный вызов
// }
// loop(0);


// newUser = undefined;

function createNewUser() {
    let name=prompt('enter your name', '');
    let surname= prompt('enter your surname');

    const newUser = {
        firstName: name,
        lastName: surname,
        getLogin: function(){
           return  this.firstName.toLowerCase()+this.lastName.toLowerCase()
        }
    };
    return newUser
    }

const newUser = createNewUser();

console.log(newUser.getLogin());
